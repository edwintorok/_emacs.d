default: custom.elc init.elc

%.elc: %.el
	rm -f $@
	emacs -Q --batch --eval '(byte-compile-file "$<")'


init.el: init.org
	emacs -Q --batch --load org --eval '(org-babel-tangle-file "init.org")'
	xrdb -merge .Xresources
