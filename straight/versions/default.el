(("Fill-Column-Indicator" . "d2536b1c48f78679e15a2b50cd5d8c0ffde4b155")
 ("Highlight-Indentation-for-Emacs" . "35e2c1d4f8f368685893128f77f90454cb9c2708")
 ("all-the-icons.el" . "52d1f2d36468146c93aaf11399f581401a233306")
 ("apiwrap.el" . "e4c9c57d6620a788ec8a715ff1bb50542edea3a6")
 ("auto-compile" . "6ce4255ab9a0b010ef8414c5bd9a6d6d9eea012f")
 ("company-mode" . "afbaad1527e6bd315237fa6db2e0caedc61e495d")
 ("dash.el" . "6514359b8606a6a9a94068ccd601fcd6379d6584")
 ("diff-hl" . "154c64affe7bdd16da814d198277d29bd1b6bb2a")
 ("doom-modeline" . "19757b4a99e6a633b30aac2677fd8c2ced1ddfea")
 ("eldoc-eval" . "f59a1ae7ecfa97ef659c7adb93e0673419acc485")
 ("elisp-refs" . "226ea71373930a5b4a8b4e45243ac16f58bb739c")
 ("elpa" . "ed762233142b27589202b70cfe32bbd8d3e5b8ae")
 ("elpy" . "763f75bb3a078fa7e9dc40280872634fcbc9092f")
 ("emacs-anzu" . "e6c56ca8b23ac433f7be58b6f3f50801dd4164e4")
 ("emacs-async" . "d17c11e6082aa51f421bb037b828bdb15f405618")
 ("emacs-evil-anzu" . "9bca6ca14d865e7e005bc02a28a09b4ae74facc9")
 ("emacs-memoize" . "9a561268ffb550b257a08710489a95cd087998b6")
 ("emacs-which-key" . "c938bbf8d4b506d8a16bedf0059703236ce05a50")
 ("epkgs" . "46c392100d7d79d803bcc46250c4fa4e176f6584")
 ("epl" . "78ab7a85c08222cd15582a298a364774e3282ce6")
 ("evil" . "6fde982d731e2cc4e5f6bded6f8955ab2daee3b7")
 ("evil-magit" . "9e2275b14807168451e10b93d69e420e435f21ef")
 ("evil-org-mode" . "b6d652a9163d3430a9e0933a554bdbee5244bbf6")
 ("f.el" . "de6d4d40ddc844eee643e92d47b9d6a63fbebb48")
 ("find-file-in-project" . "83c9384e0c85ee8e0e4ad79d13a24181b43ae0b0")
 ("flx" . "46040d0b096a0340d91235561f27a959a61d0fef")
 ("flycheck" . "f85eb1c8f1aeb594ce71a048a86bc3fb5e590c4b")
 ("flycheck-ocaml" . "8707a7bf545a8639a6a5c600a98d9a2ea1487dc9")
 ("flycheck-pos-tip" . "909113977d37739387c7f099d74a724cfe6efcec")
 ("general.el" . "f48c43c4449677fa629aac2693ffcb850ca58c89")
 ("ghub" . "84dbc5d1474302dc336a359480615dc8c40a47c5")
 ("ghub-plus" . "681a2dc34f6bc41df8d50b60f1dfd565aae6573a")
 ("golden-ratio.el" . "72b028808b41d23fa3f7e8c0d23d2c475e7b46ae")
 ("goto-chg" . "e5b38e4e1378f6ea48fa9e8439f49c2998654aa4")
 ("graphql.el" . "e2b309689f4faf9225f290080f836e988c5a576d")
 ("helm" . "46157453fee320658871c4ca4e0c57e80de77b42")
 ("helm-flx" . "6640fac5cb16bee73c95b8ed1248a4e5e113690e")
 ("helm-projectile" . "8a2dbc973548fac89356c11d70f7f474ea1367a5")
 ("helm-rg" . "d356a2abb6359d709487ed49414e151627287577")
 ("helpful" . "1c1c3240dc2a95982d3dacc74b49ce2bcd1ee81e")
 ("highlight-escape-sequences" . "08d846a7aa748209d65fecead2b6a766c3e5cb41")
 ("hl-todo" . "b1b1b693acd3dc71fcc41a553481c21a734ba7d9")
 ("iedit" . "a1a2abbfc34bf08bb156bee5c21b6d85dc92a9b3")
 ("loop.el" . "e22807f83a0890dc8a904c51ee0742c34efccc6c")
 ("magit" . "3d033e6f1847bcd67a5a58fa67f79c07a06ead7a")
 ("magit-popup" . "8436447e3166b797edc596cf220f3bf9b41ff4d0")
 ("magithub" . "ec84425d7d99ccd9d7e3c51b3cfeba349d064c9c")
 ("markdown-mode" . "30ae22215da05c4e02fcc3bfee0317cfec9c8fe5")
 ("melpa" . "d22ebb5ef16df0c56d6031cb1c7f312dca514482")
 ("merlin" . "e14597b2a619a655b1939f51410d305ed5af3ab2")
 ("merlin-eldoc" . "85dec436648f43c050048524fae7a3ad7ad4c019")
 ("mixed-pitch" . "f9bcdd9e30f8370ef0607d714b9411eddf8dd866")
 ("multi-term" . "f954e4e18b0a035151d34852387e724d87a3316f")
 ("no-littering" . "3f6d290bb43d75ba749d56fffc21c15f1d4157d2")
 ("ocaml" . "fcb68de43b5cd257ea40c80c5b0091b63668c103")
 ("ocp-indent" . "ec94fb2a2e163f190696bd72631d44fd6d67a6a4")
 ("org" . "4a2e7c8db9463e7c85ae81256f551e56ef4abd05")
 ("org-bullets" . "b56f2e3812626f2c4ac1686073d102c71f4a8513")
 ("org-noter" . "2ac0536314d3e14c3f8d98c6478068c3731c2cd5")
 ("packed" . "c41c3dfda86ae33832ffc146923e2a4675cbacfa")
 ("paren-face" . "a45d111153a76c481fa0b36d6172ac90e073dfc4")
 ("pdf-tools" . "deb93df9b555b872db4595b6f47645a93eeb280f")
 ("pkg-info.el" . "76ba7415480687d05a4353b27fea2ae02b8d9d61")
 ("popup-el" . "80829dd46381754639fb764da11c67235fe63282")
 ("pos-tip" . "051e08fec5cf30b7574bdf439f79fef7d42d689d")
 ("projectile" . "6cb9524b67a4041def7c6d065608032d21eddf02")
 ("pyvenv" . "921ae2356b6a111ac0b7e44fd04cba8e95cbe936")
 ("rainbow-delimiters" . "19b93892afa0494ba749c2ca9c154e04447ad778")
 ("s.el" . "03410e6a7a2b11e47e1fea3b7d9899c7df26435e")
 ("shrink-path.el" . "9d06c453d1537df46a4b703a29213cc7f7857aa0")
 ("shut-up" . "081d6b01e3ba0e60326558e545c4019219e046ce")
 ("straight.el" . "3162136026660f3c9ab9e96a3a81b2a99f2612c0")
 ("swiper" . "d3469919cdfdd6165a83b818df08feea50181012")
 ("swiper-helm" . "93fb6db87bc6a5967898b5fd3286954cc72a0008")
 ("tablist" . "c834a84efb6efa32497efe1e73160fade741b836")
 ("toc-org" . "c83c19ddb4a5f4bfcc38203430f430c37cee9c14")
 ("treepy.el" . "b40e6b09eb9be45da67b8c9e4990a5a0d7a2a09d")
 ("tuareg" . "c030be472c2aea4d4b770705f9099fb00e952618")
 ("use-package" . "6c6ecca2aac0b809b9f813e83c897caae01b335d")
 ("utop" . "ea38850e606dd18c94e2ccabc28485fec1c8f91f")
 ("volatile-highlights.el" . "9a20091f0ce7fc0a6b3e641a6a46d5f3ac4d8392")
 ("with-editor" . "f69daaf0acd9096bd3758fdc73a34b1a48d9c4f1")
 ("yasnippet" . "9f51cf29c945596a6d8f3075caf748e9130d84b1")
 ("yasnippet-snippets" . "88e209997a93f290206bb6e8c7c81d03307ae486")
 ("zenburn-emacs" . "d71a0f0556c1db785738ab9b0c989df342705a81"))
:saturn
